const mongo = require('mongoose');
const Promise = require('bluebird');
const config = require('./index');

// plug-in bkuebird as mongoose Promise
mongo.Promise = Promise;

// connect to mongo host, retry on initial fail 

const connectMongo = () => {
    mongo.connect(config.mongo.host, config.mongo.options)
        .catch( err => {
            console.log('connection faild')
            setTimeout(connectMongo, 2000);
        })
}


// initialize mongo connection

const init = () => {
    connectMongo();
    
    mongo.connection.on('connected', () => {
        console.log(`connected to db ${config.mongo.host}`)
    })

    mongo.connection.on('error', err => {
        console.log('mongo error', err.message || err);
    })

}

module.exports = init;