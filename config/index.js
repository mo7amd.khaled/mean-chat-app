// set configs from local env file

const NODE_ENV = process.env.NODE_ENV || 'development';
const PORT = process.env.PORT || 3000;
const MONGO_HOST = process.env.MONGO_HOST || 'mongodb://localhost:27017/chat-app';
const SECRET = process.env.SECRET || 'open';
const ROOT = process.env.ROOT || '';
const FACEBOOK_ID = process.env.FACEBOOK_ID || 'app_id'
const FACEBOOK_SECRET = process.env.FACEBOOK_SECRET || '*********';
const FACEBOOK_CALLBACK = process.env.FACEBOOK_CALLBACK || 'http://localhost:3000/login/facebook/callback';

// initialize config obj containing the app setting

const config = {
    env: NODE_ENV,
    root: ROOT,
    server: {
        port: PORT
    },
    mongo: {
        host: MONGO_HOST,
        secret: SECRET,
        options: {
            reconnectTries: Number.MAX_VALUE
        }
    },
    facebook: {
        appId: FACEBOOK_ID,
        appSecret: FACEBOOK_SECRET,
        callbackUrl: FACEBOOK_CALLBACK
    }
}

module.exports = config;