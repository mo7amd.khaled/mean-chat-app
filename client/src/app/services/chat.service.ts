import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import * as io from 'socket.io-client';
import { Message } from '../shared/message.interface';
import { AuthService } from './auth.service';
import {environment} from '../../environments/environment';

const BASE_URL = environment.backendUrl;

@Injectable()
export class ChatService {
  private socket: any;
  private chatUrl: string = environment.chatUrl;
  private apiUrl: String = `${BASE_URL}/messages`;
  private usersUrl: String = `${BASE_URL}/users`;

  constructor(private authService: AuthService, private http: Http) { }

  connect(username: String, callback: Function = () => {}): void {
    // initialize the connection
    this.socket = io(this.chatUrl, { path: '/chat' });

    this.socket.on('error', (error) => {
      console.log('====================================');
      console.log(error);
      console.log('====================================');
    });

    this.socket.on('connect', () => {
      this.sendUser(username);
      console.log('connected to the chat server');
      callback();
    });
  }

  isConnected(): boolean {
    if (this.socket != null) {
      return true;
    } else {
      return false;
    }
  }

  sendUser(username: String): void {
    this.socket.emit('username', {username: username});
  }

  disconnect(): void {
    this.socket.disconnect();
  }

  getConversation(name1: String, name2: String): any {
    let url = this.apiUrl as string;
    if (name2 !== 'chat-room') {
      const route = '/' + name1 + '/' + name2;
      url += route;
    }

    const authToken = this.authService.getUserData().token;

    // prepare the request
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });
    const options = new RequestOptions({ headers: headers });

    // POST
    const observableReq = this.http.get(url, options)
                                 .map(this.extractData);

    return observableReq;
  }

  getUserList(): any {
    const url = this.usersUrl as string;

    const authToken = this.authService.getUserData().token;

    // prepare the request
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': authToken
    });
    const options = new RequestOptions({ headers: headers });

    // POST
    const observableReq = this.http.get(url, options)
                                 .map(this.extractData);

    return observableReq;
  }

  receiveMessage(): any {
    const observable = new Observable(observer => {
      this.socket.on('message', (data: Message) => {
        observer.next(data);
      });
    });

    return observable;
  }

  receiveActiveList(): any {
    const observable = new Observable(observer => {
      this.socket.on('active', (data) => {
        observer.next(data);
      });
    });

    return observable;
  }

  sendMessage(message: Message, chatWith: String): void {
    this.socket.emit('message', {message: message, to: chatWith});
  }

  getActiveList(): void {
    this.socket.emit('getactive');
  }

  extractData(res: Response): any {
    const body = res.json();
    return body || { };
  }

}
