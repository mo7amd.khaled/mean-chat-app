import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import {environment} from '../../environments/environment';
import 'rxjs/add/operator/map';

const BASE_URL = environment.backendUrl;

@Injectable()
export class AuthService {
  private authToken: string;
  private user: string;

  private apiUrl: String = `${BASE_URL}/users`;

  constructor(private http: Http) { }

  registerUser(user): any {
    const url: string = this.apiUrl + '/register';

    // prepare the request
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const reqBody = user;

    // POST
    const observableReq = this.http.post(url, reqBody, options).map(this.extractData);

    return observableReq;
  }

  authenticateUser(user): any {
    const url: string = this.apiUrl + '/authenticate';

    // prepare the request
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    const reqBody = user;

    // POST
    const observableReq = this.http.post(url, reqBody, options).map(this.extractData);

    return observableReq;
  }

  getProfile(): any {
    const url: string = this.apiUrl + '/profile';
    this.loadCredentials();

    // prepare the request
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.authToken
    });
    const options = new RequestOptions({ headers: headers });

    // POST
    const observableReq = this.http.get(url, options).map(this.extractData);

    return observableReq;
  }

  storeUserData(token, user): void {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  getUserData(): any {
    this.loadCredentials();
    const jUser = JSON.parse(this.user);
    const jData = {token: this.authToken, user: jUser};

    return jData;
  }

  loadCredentials(): void {
    const token = localStorage.getItem('token');
    const user = localStorage.getItem('user');
    this.authToken = token;
    this.user = user;
  }

  loggedIn(): boolean {
    return tokenNotExpired();
  }

  logout(): void {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  extractData(res: Response): any {
    const body = res.json();
    return body || { };
  }

}
