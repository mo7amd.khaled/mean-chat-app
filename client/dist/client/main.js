(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/active-list/active-list.component.html":
/*!********************************************************!*\
  !*** ./src/app/active-list/active-list.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\">\n  <h3 class=\"text-center\">Chats</h3>\n\n  <ul class=\"list-group\">\n    <a href=\"#\" class=\"list-group-item\" (click)=\"onUserClick('chat-room')\" [ngClass]=\"{active: current == 'chat-room'}\">\n      <span class=\"glyphicon glyphicon-comment\" aria-hidden=\"true\" style=\"margin-right: 2px\"></span>\n      Chat Room\n    </a>\n    <a *ngFor=\"let user of users\"\n      href=\"#\"\n      class=\"list-group-item\"\n      (click)=\"onUserClick(user.username)\"\n      [ngClass]=\"{active: user.username == current, online: user.online}\"\n      >\n        <img src=\"assets/img/generic-avatar.png\" alt=\"avatar\" height=\"28.5px\" width=\"25px\">\n        {{user.username}}\n        <span *ngIf=\"user.online\" class=\"glyphicon glyphicon-ok-sign pull-right\" aria-hidden=\"true\" style=\"padding-top: 5px\"></span>\n    </a>\n  </ul>\n\n</div>\n"

/***/ }),

/***/ "./src/app/active-list/active-list.component.scss":
/*!********************************************************!*\
  !*** ./src/app/active-list/active-list.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul.list-group {\n  margin-top: 24px;\n  margin-bottom: 0; }\n  ul.list-group a.list-group-item {\n    border-right: none;\n    border-left: none; }\n  ul.list-group a.list-group-item span.glyphicon-comment {\n      color: #4CAF50; }\n  ul.list-group a.list-group-item.online span.glyphicon {\n      color: #4CAF50; }\n  ul.list-group a.list-group-item.online img {\n      border: 2px solid #4CAF50; }\n"

/***/ }),

/***/ "./src/app/active-list/active-list.component.ts":
/*!******************************************************!*\
  !*** ./src/app/active-list/active-list.component.ts ***!
  \******************************************************/
/*! exports provided: ActiveListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActiveListComponent", function() { return ActiveListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ActiveListComponent = /** @class */ (function () {
    function ActiveListComponent() {
        this.newConv = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ActiveListComponent.prototype.ngOnInit = function () {
    };
    ActiveListComponent.prototype.onUserClick = function (username) {
        this.newConv.emit(username);
        return false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], ActiveListComponent.prototype, "users", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], ActiveListComponent.prototype, "current", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ActiveListComponent.prototype, "newConv", void 0);
    ActiveListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-active-list',
            template: __webpack_require__(/*! ./active-list.component.html */ "./src/app/active-list/active-list.component.html"),
            styles: [__webpack_require__(/*! ./active-list.component.scss */ "./src/app/active-list/active-list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ActiveListComponent);
    return ActiveListComponent;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\n  <!-- Navbar -->\n  <app-navbar></app-navbar>\n</header>\n\n<div class=\"\">\n  <!-- Alerts -->\n  <div class=\"container\">\n    <flash-messages></flash-messages>\n  </div>\n\n  <!-- Page Content -->\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./sign-up/sign-up.component */ "./src/app/sign-up/sign-up.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var angular2_flash_messages_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angular2-flash-messages/module */ "./node_modules/angular2-flash-messages/module/index.js");
/* harmony import */ var angular2_flash_messages_module__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages_module__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/auth.guard.service */ "./src/app/services/auth.guard.service.ts");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/chat.service */ "./src/app/services/chat.service.ts");
/* harmony import */ var _chat_room_chat_room_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./chat-room/chat-room.component */ "./src/app/chat-room/chat-room.component.ts");
/* harmony import */ var _active_list_active_list_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./active-list/active-list.component */ "./src/app/active-list/active-list.component.ts");
/* harmony import */ var _message_message_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./message/message.component */ "./src/app/message/message.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var appRoutes = [
    { path: '', component: _login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"] },
    { path: 'register', component: _sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_7__["SignUpComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"] },
    { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_16__["ProfileComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__["AuthGuard"]] },
    { path: 'chat', canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__["AuthGuard"]], children: [
            { path: ':chatWith', component: _chat_room_chat_room_component__WEBPACK_IMPORTED_MODULE_13__["ChatRoomComponent"] },
            { path: '**', redirectTo: 'chat/chat-room', pathMatch: 'full' }
        ] },
    { path: '**', redirectTo: 'chat/chat-room', pathMatch: 'full' }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"],
                _sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_7__["SignUpComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__["NavbarComponent"],
                _chat_room_chat_room_component__WEBPACK_IMPORTED_MODULE_13__["ChatRoomComponent"],
                _active_list_active_list_component__WEBPACK_IMPORTED_MODULE_14__["ActiveListComponent"],
                _message_message_component__WEBPACK_IMPORTED_MODULE_15__["MessageComponent"],
                _profile_profile_component__WEBPACK_IMPORTED_MODULE_16__["ProfileComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_3__["HttpModule"],
                angular2_flash_messages_module__WEBPACK_IMPORTED_MODULE_9__["FlashMessagesModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(appRoutes)
            ],
            providers: [
                _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_11__["AuthGuard"],
                _services_auth_service__WEBPACK_IMPORTED_MODULE_10__["AuthService"],
                _services_chat_service__WEBPACK_IMPORTED_MODULE_12__["ChatService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/chat-room/chat-room.component.html":
/*!****************************************************!*\
  !*** ./src/app/chat-room/chat-room.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col-sm-3 col-lg-2 active-list-container\" [ngClass]=\"{open: showActive}\">\n      <app-active-list [users]=\"userList\" [current]=\"chatWith\" (newConv)=\"onNewConv($event)\"></app-active-list>\n    </div>\n\n    <div class=\"col-sm-9 col-lg-10 column\">\n      <div class=\"panel panel-primary chat-container\">\n        <div class=\"panel-heading text-center clearfix\">\n          <button class=\"btn btn-warning btn-sm pull-left hidden-atw\" type=\"button\" name=\"active\" (click)=\"onUsersClick()\"><</button>\n          {{chatWith}} <span *ngIf=\"currentOnline\">online</span>\n        </div>\n        <div class=\"panel-body msg-container\" [ngClass]=\"{blurred: showActive}\">\n          <div *ngIf=\"noMsg\" id=\"noMsg\" class=\"text-center\">\n            There are no messages\n          </div>\n          <app-message *ngFor=\"let message of messageList\" [message]=\"message\"></app-message>\n        </div>\n\n        <div class=\"panel-footer\" [ngClass]=\"{blurred: showActive}\">\n          <form [formGroup]=\"sendForm\"\n            (ngSubmit)=\"onSendSubmit()\"\n            >\n\n            <div class=\"input-group\">\n              <input type=\"text\"\n                class=\"form-control\"\n                placeholder=\"Type your message...\"\n                id=\"message\"\n                name=\"message\"\n                formControlName=\"message\"\n                autofocus=\"true\"\n                autocomplete=\"off\"\n                >\n\n              <span class=\"input-group-btn\">\n                <button [disabled]=\"sendForm.invalid\" type=\"submit\" class=\"btn btn-primary\" name=\"send\">Send</button>\n              </span>\n            </div>\n          </form>\n        </div>\n\n        <div *ngIf=\"notify\" class=\"well notification\">\n          <h4><strong>{{notification.from}}</strong> <span *ngIf=\"notification.inChatRoom\">to chat-room</span>:</h4>\n          <p>{{notification.text}}</p>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</div>\n\n<audio id=\"notifSound\" src=\"assets/audio/notification.mp3\" type=\"audio/mpeg\">\n<audio id=\"msgSound\" src=\"assets/audio/message.mp3\" type=\"audio/mpeg\">\n\n</audio>\n"

/***/ }),

/***/ "./src/app/chat-room/chat-room.component.scss":
/*!****************************************************!*\
  !*** ./src/app/chat-room/chat-room.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* -------------------------------------------------------------\n  Sass CSS3 Mixins! The Cross-Browser CSS3 Sass Library\n  By: Matthieu Aussaguel, http://www.mynameismatthieu.com, @matthieu_tweets\n\n  List of CSS3 Sass Mixins File to be @imported and @included as you need\n\n  The purpose of this library is to facilitate the use of CSS3 on different browsers avoiding HARD TO READ and NEVER\n  ENDING css files\n\n  note: All CSS3 Properties are being supported by Safari 5\n  more info: http://www.findmebyip.com/litmus/#css3-properties\n\n------------------------------------------------------------- */\n/**\n* SassFlexbox\n* Manage Flexbox in Sass easily.\n*\n* @author     Samuel Marchal (zessx)\n* @version    0.1\n*/\n/*\n    Display\n */\n/*\n    Flex direction\n */\n/*\n    Flex wrap\n */\n/*\n    Flex flow\n */\n/*\n    Order\n */\n/*\n    Flex grow\n */\n/*\n    Flex shrink\n */\n/*\n    Flex basis\n */\n/*\n    Flex\n */\n/*\n    Justify content\n */\n/*\n    Align items\n */\n/*\n    Align self\n */\n/*\n    Align content\n */\n.column {\n  padding-left: 0;\n  padding-right: 0; }\n.active-list-container {\n  padding-left: 4px;\n  padding-right: 4px; }\n.chat-container {\n  border: none;\n  margin-bottom: 0;\n  -ms-box-shadow: -2px 0px 5px rgba(0, 0, 0, 0.4);\n  -o-box-shadow: -2px 0px 5px rgba(0, 0, 0, 0.4);\n  box-shadow: -2px 0px 5px rgba(0, 0, 0, 0.4);\n  border-radius: 0;\n  background-color: #f2f2f3; }\n.chat-container .panel-heading {\n    height: 40px;\n    padding: 8px 15px;\n    margin: 0;\n    margin-bottom: 4px;\n    border: none;\n    -webkit-border-top-left-radius: 0;\n    -webkit-border-top-right-radius: 0;\n    -webkit-border-bottom-right-radius: 5px;\n    -webkit-border-bottom-left-radius: 5px;\n    -moz-border-radius-topleft: 0;\n    -moz-border-radius-topright: 0;\n    -moz-border-radius-bottomright: 5px;\n    -moz-border-radius-bottomleft: 5px;\n    border-top-left-radius: 0;\n    border-top-right-radius: 0;\n    border-bottom-right-radius: 5px;\n    border-bottom-left-radius: 5px;\n    background-color: whitesmoke;\n    color: black;\n    -ms-box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.4);\n    -o-box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.4);\n    box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.4); }\n.chat-container .panel-heading span {\n      font-size: 12px;\n      color: #BDBDBD; }\n.chat-container .panel-heading button {\n      padding: 6px 10px;\n      margin-top: -5px; }\n.chat-container .msg-container {\n    margin: 0;\n    border: none;\n    height: calc(100vh - 171px);\n    overflow-y: scroll;\n    transition: -webkit-filter .2s ease-in 0s;\n    transition: filter .2s ease-in 0s;\n    transition: filter .2s ease-in 0s, -webkit-filter .2s ease-in 0s; }\n.chat-container .panel-footer {\n    transition: -webkit-filter .2s ease-in 0s;\n    transition: filter .2s ease-in 0s;\n    transition: filter .2s ease-in 0s, -webkit-filter .2s ease-in 0s; }\n.chat-container .panel-footer form .input-group {\n      width: 100%; }\n.blurred {\n  -webkit-filter: blur(2px);\n  -khtml-filter: blur(2px);\n  -moz-filter: blur(2px);\n  -ms-filter: blur(2px);\n  -o-filter: blur(2px);\n  filter: blur(2px); }\n#noMsg {\n  width: 100%;\n  height: 100%;\n  padding-top: calc((100vh - 171px) / 2 - 30px); }\n.notification {\n  position: absolute;\n  padding: 15px;\n  margin: 0;\n  top: 24px;\n  right: 10px;\n  z-index: 3;\n  width: 320px;\n  background-color: #E8F5E9;\n  border: 2px solid #18bc9c;\n  border-radius: 10px;\n  -ms-box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.4);\n  -o-box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.4);\n  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.4);\n  -webkit-animation: notification .4s ease-in-out 0s 1;\n  animation: notification .4s ease-in-out 0s 1; }\n.notification h4 {\n    margin-top: 0; }\n.notification h4 span {\n      font-size: 16px; }\n.notification p {\n    margin-left: 4px;\n    font-size: 14px; }\n@-webkit-keyframes notification {\n  0% {\n    filter: alpha(opacity=0);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\";\n    -webkit-opacity: 0;\n    -khtml-opacity: 0;\n    -moz-opacity: 0;\n    -ms-opacity: 0;\n    -o-opacity: 0;\n    opacity: 0;\n    -webkit-transform: translateX(330px);\n    -khtml-transform: translateX(330px);\n    transform: translateX(330px); }\n  80% {\n    filter: alpha(opacity=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -webkit-opacity: 1;\n    -khtml-opacity: 1;\n    -moz-opacity: 1;\n    -ms-opacity: 1;\n    -o-opacity: 1;\n    opacity: 1;\n    -webkit-transform: translateX(-12px);\n    -khtml-transform: translateX(-12px);\n    transform: translateX(-12px); }\n  85% {\n    filter: alpha(opacity=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -webkit-opacity: 1;\n    -khtml-opacity: 1;\n    -moz-opacity: 1;\n    -ms-opacity: 1;\n    -o-opacity: 1;\n    opacity: 1;\n    -webkit-transform: translateX(2px);\n    -khtml-transform: translateX(2px);\n    transform: translateX(2px); }\n  100% {\n    filter: alpha(opacity=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -webkit-opacity: 1;\n    -khtml-opacity: 1;\n    -moz-opacity: 1;\n    -ms-opacity: 1;\n    -o-opacity: 1;\n    opacity: 1;\n    -webkit-transform: translateX(0);\n    -khtml-transform: translateX(0);\n    transform: translateX(0); } }\n@keyframes notification {\n  0% {\n    filter: alpha(opacity=0);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\";\n    -webkit-opacity: 0;\n    -khtml-opacity: 0;\n    -moz-opacity: 0;\n    -ms-opacity: 0;\n    -o-opacity: 0;\n    opacity: 0;\n    -webkit-transform: translateX(330px);\n    -khtml-transform: translateX(330px);\n    transform: translateX(330px); }\n  80% {\n    filter: alpha(opacity=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -webkit-opacity: 1;\n    -khtml-opacity: 1;\n    -moz-opacity: 1;\n    -ms-opacity: 1;\n    -o-opacity: 1;\n    opacity: 1;\n    -webkit-transform: translateX(-12px);\n    -khtml-transform: translateX(-12px);\n    transform: translateX(-12px); }\n  85% {\n    filter: alpha(opacity=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -webkit-opacity: 1;\n    -khtml-opacity: 1;\n    -moz-opacity: 1;\n    -ms-opacity: 1;\n    -o-opacity: 1;\n    opacity: 1;\n    -webkit-transform: translateX(2px);\n    -khtml-transform: translateX(2px);\n    transform: translateX(2px); }\n  100% {\n    filter: alpha(opacity=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -webkit-opacity: 1;\n    -khtml-opacity: 1;\n    -moz-opacity: 1;\n    -ms-opacity: 1;\n    -o-opacity: 1;\n    opacity: 1;\n    -webkit-transform: translateX(0);\n    -khtml-transform: translateX(0);\n    transform: translateX(0); } }\n@media only screen and (max-width: 767.99px) {\n  .active-list-container {\n    display: none;\n    position: absolute;\n    top: 100px;\n    left: 0;\n    z-index: 3;\n    margin: 0;\n    height: calc(100vh - 100px);\n    width: 220px;\n    padding-top: 21px;\n    background-color: #ffffff;\n    -ms-box-shadow: 2px 0px 5px rgba(0, 0, 0, 0.4);\n    -o-box-shadow: 2px 0px 5px rgba(0, 0, 0, 0.4);\n    box-shadow: 2px 0px 5px rgba(0, 0, 0, 0.4); }\n    .active-list-container.open {\n      display: block; }\n  .notification {\n    top: -52px;\n    right: 8vw;\n    left: 8vw;\n    width: 84vw;\n    z-index: 1001;\n    -webkit-animation: notification-mobile .4s ease-out 0s 1;\n    animation: notification-mobile .4s ease-out 0s 1; } }\n@-webkit-keyframes notification-mobile {\n  0% {\n    filter: alpha(opacity=0);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\";\n    -webkit-opacity: 0;\n    -khtml-opacity: 0;\n    -moz-opacity: 0;\n    -ms-opacity: 0;\n    -o-opacity: 0;\n    opacity: 0;\n    -webkit-transform: translateY(-200px);\n    -khtml-transform: translateY(-200px);\n    transform: translateY(-200px); }\n  100% {\n    filter: alpha(opacity=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -webkit-opacity: 1;\n    -khtml-opacity: 1;\n    -moz-opacity: 1;\n    -ms-opacity: 1;\n    -o-opacity: 1;\n    opacity: 1;\n    -webkit-transform: translateX(0);\n    -khtml-transform: translateX(0);\n    transform: translateX(0); } }\n@keyframes notification-mobile {\n  0% {\n    filter: alpha(opacity=0);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\";\n    -webkit-opacity: 0;\n    -khtml-opacity: 0;\n    -moz-opacity: 0;\n    -ms-opacity: 0;\n    -o-opacity: 0;\n    opacity: 0;\n    -webkit-transform: translateY(-200px);\n    -khtml-transform: translateY(-200px);\n    transform: translateY(-200px); }\n  100% {\n    filter: alpha(opacity=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -webkit-opacity: 1;\n    -khtml-opacity: 1;\n    -moz-opacity: 1;\n    -ms-opacity: 1;\n    -o-opacity: 1;\n    opacity: 1;\n    -webkit-transform: translateX(0);\n    -khtml-transform: translateX(0);\n    transform: translateX(0); } }\n"

/***/ }),

/***/ "./src/app/chat-room/chat-room.component.ts":
/*!**************************************************!*\
  !*** ./src/app/chat-room/chat-room.component.ts ***!
  \**************************************************/
/*! exports provided: ChatRoomComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatRoomComponent", function() { return ChatRoomComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/chat.service */ "./src/app/services/chat.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChatRoomComponent = /** @class */ (function () {
    function ChatRoomComponent(route, router, formBuilder, el, authService, chatService) {
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.el = el;
        this.authService = authService;
        this.chatService = chatService;
        this.messageList = [];
        this.notification = { timeout: null };
    }
    ChatRoomComponent.prototype.ngOnInit = function () {
        var _this = this;
        var userData = this.authService.getUserData();
        this.username = userData.user.username;
        this.route.params.subscribe(function (params) {
            _this.chatWith = params.chatWith;
        });
        this.sendForm = this.formBuilder.group({
            message: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
        this.getMessages(this.chatWith);
        this.connectToChat();
    };
    ChatRoomComponent.prototype.ngOnDestroy = function () {
        this.receiveActiveObs.unsubscribe();
        this.receiveMessageObs.unsubscribe();
    };
    ChatRoomComponent.prototype.connectToChat = function () {
        var _this = this;
        var connected = this.chatService.isConnected();
        if (connected === true) {
            this.initReceivers();
        }
        else {
            this.chatService.connect(this.username, function () {
                _this.initReceivers();
            });
        }
    };
    ChatRoomComponent.prototype.getMessages = function (name) {
        var _this = this;
        this.chatService.getConversation(this.username, name)
            .subscribe(function (data) {
            if (data.success === true) {
                _this.conversationId = data.conversation._id || data.conversation._doc._id;
                var messages = data.conversation.messages || null;
                if (messages && messages.length > 0) {
                    for (var _i = 0, messages_1 = messages; _i < messages_1.length; _i++) {
                        var message = messages_1[_i];
                        _this.checkMine(message);
                    }
                    _this.noMsg = false;
                    _this.messageList = messages;
                    _this.scrollToBottom();
                }
                else {
                    _this.noMsg = true;
                    _this.messageList = [];
                }
            }
            else {
                _this.onNewConv('chat-room');
            }
        });
    };
    ChatRoomComponent.prototype.getUserList = function () {
        var _this = this;
        this.chatService.getUserList()
            .subscribe(function (data) {
            if (data.success === true) {
                var users = data.users;
                for (var i = 0; i < users.length; i++) {
                    if (users[i].username === _this.username) {
                        users.splice(i, 1);
                        break;
                    }
                }
                _this.userList = users.sort(_this.compareByUsername);
                _this.receiveActiveObs = _this.chatService.receiveActiveList()
                    .subscribe(function (activeUsers) {
                    for (var _i = 0, activeUsers_1 = activeUsers; _i < activeUsers_1.length; _i++) {
                        var onlineUser = activeUsers_1[_i];
                        if (onlineUser.username !== _this.username) {
                            var flaggy = 0;
                            for (var _a = 0, _b = _this.userList; _a < _b.length; _a++) {
                                var registered = _b[_a];
                                if (registered.username === onlineUser.username) {
                                    flaggy = 1;
                                    break;
                                }
                            }
                            if (flaggy === 0) {
                                _this.userList.push(onlineUser);
                                _this.userList.sort(_this.compareByUsername);
                            }
                        }
                    }
                    for (var _c = 0, _d = _this.userList; _c < _d.length; _c++) {
                        var user = _d[_c];
                        var flag = 0;
                        for (var _e = 0, activeUsers_2 = activeUsers; _e < activeUsers_2.length; _e++) {
                            var liveUser = activeUsers_2[_e];
                            if (liveUser.username === user.username) {
                                user.online = true;
                                flag = 1;
                                break;
                            }
                        }
                        if (flag === 0) {
                            user.online = false;
                        }
                    }
                    _this.currentOnline = _this.checkOnline(_this.chatWith);
                });
                _this.chatService.getActiveList();
            }
            else {
                _this.onNewConv('chat-room');
            }
        });
    };
    ChatRoomComponent.prototype.initReceivers = function () {
        var _this = this;
        this.getUserList();
        this.receiveMessageObs = this.chatService.receiveMessage()
            .subscribe(function (message) {
            _this.checkMine(message);
            if (message.conversationId === _this.conversationId) {
                _this.noMsg = false;
                _this.messageList.push(message);
                _this.scrollToBottom();
                _this.msgSound();
            }
            else if (message.mine !== true) {
                if (_this.notification.timeout) {
                    clearTimeout(_this.notification.timeout);
                }
                _this.notification = {
                    from: message.from,
                    inChatRoom: message.inChatRoom,
                    text: message.text,
                    timeout: setTimeout(function () { _this.notify = false; }, 4000)
                };
                _this.notify = true;
                _this.notifSound();
            }
        });
    };
    ChatRoomComponent.prototype.onSendSubmit = function () {
        var newMessage = {
            created: new Date(),
            from: this.username,
            text: this.sendForm.value.message,
            conversationId: this.conversationId,
            inChatRoom: this.chatWith === 'chat-room'
        };
        this.chatService.sendMessage(newMessage, this.chatWith);
        newMessage.mine = true;
        this.noMsg = false;
        this.messageList.push(newMessage);
        this.scrollToBottom();
        this.msgSound();
        this.sendForm.setValue({ message: '' });
    };
    ChatRoomComponent.prototype.checkMine = function (message) {
        if (message.from === this.username) {
            message.mine = true;
        }
    };
    ChatRoomComponent.prototype.onUsersClick = function () {
        this.showActive = !this.showActive;
    };
    ChatRoomComponent.prototype.onNewConv = function (username) {
        if (this.chatWith !== username) {
            this.router.navigate(['/chat', username]);
            this.getMessages(username);
        }
        else {
            this.getMessages(username);
        }
        this.currentOnline = this.checkOnline(username);
        this.showActive = false;
    };
    ChatRoomComponent.prototype.notifSound = function () {
        var sound = this.el.nativeElement.querySelector('#notifSound');
        sound.play();
    };
    ChatRoomComponent.prototype.msgSound = function () {
        var sound = this.el.nativeElement.querySelector('#msgSound');
        sound.load();
        sound.play();
    };
    ChatRoomComponent.prototype.scrollToBottom = function () {
        var element = this.el.nativeElement.querySelector('.msg-container');
        setTimeout(function () {
            element.scrollTop = element.scrollHeight;
        }, 100);
    };
    ChatRoomComponent.prototype.checkOnline = function (name) {
        if (name === 'chat-room') {
            for (var _i = 0, _a = this.userList; _i < _a.length; _i++) {
                var user = _a[_i];
                if (user.online === true) {
                    return true;
                }
            }
            return false;
        }
        else {
            for (var _b = 0, _c = this.userList; _b < _c.length; _b++) {
                var user = _c[_b];
                if (user.username === name) {
                    return user.online;
                }
            }
        }
    };
    ChatRoomComponent.prototype.compareByUsername = function (a, b) {
        if (a.username < b.username) {
            return -1;
        }
        if (a.username > b.username) {
            return 1;
        }
        return 0;
    };
    ChatRoomComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chat-room',
            template: __webpack_require__(/*! ./chat-room.component.html */ "./src/app/chat-room/chat-room.component.html"),
            styles: [__webpack_require__(/*! ./chat-room.component.scss */ "./src/app/chat-room/chat-room.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _services_chat_service__WEBPACK_IMPORTED_MODULE_3__["ChatService"]])
    ], ChatRoomComponent);
    return ChatRoomComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h2 class=\"page-header text-center\">Login</h2>\n\n  <form [formGroup]=\"loginForm\"\n    (ngSubmit)=\"onLoginSubmit()\"\n    >\n\n    <div class=\"form-group\">\n      <label for=\"username\">Username</label>\n\n      <input type=\"text\" name=\"username\" class=\"form-control\"\n        id=\"username\"\n        placeholder=\"Enter your username\"\n        formControlName=\"username\"\n        autofocus=\"true\"\n        >\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"password\">Password</label>\n\n      <input type=\"password\" name=\"password\" class=\"form-control\"\n        id=\"password\"\n        placeholder=\"Enter your password\"\n        formControlName=\"password\"\n        >\n    </div>\n\n    <div class=\"form-group\">\n      <button [disabled]=\"loginForm.invalid\"\n        type=\"submit\"\n        class=\"btn btn-primary btn-block\"\n        name=\"login\"\n        >Login\n      </button>\n    </div>\n\n    <div class=\"text-center\">\n      <a [routerLink]=\"['/register']\">Don't have an account?</a>\n    </div>\n\n  </form>\n\n</div>\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* -------------------------------------------------------------\n  Sass CSS3 Mixins! The Cross-Browser CSS3 Sass Library\n  By: Matthieu Aussaguel, http://www.mynameismatthieu.com, @matthieu_tweets\n\n  List of CSS3 Sass Mixins File to be @imported and @included as you need\n\n  The purpose of this library is to facilitate the use of CSS3 on different browsers avoiding HARD TO READ and NEVER\n  ENDING css files\n\n  note: All CSS3 Properties are being supported by Safari 5\n  more info: http://www.findmebyip.com/litmus/#css3-properties\n\n------------------------------------------------------------- */\n/**\n* SassFlexbox\n* Manage Flexbox in Sass easily.\n*\n* @author     Samuel Marchal (zessx)\n* @version    0.1\n*/\n/*\n    Display\n */\n.panel-body {\n  display: -webkit-flexbox;\n  display: flex; }\n/*\n    Flex direction\n */\n/*\n    Flex wrap\n */\n/*\n    Flex flow\n */\n/*\n    Order\n */\n/*\n    Flex grow\n */\n/*\n    Flex shrink\n */\n/*\n    Flex basis\n */\n/*\n    Flex\n */\n/*\n    Justify content\n */\n.panel-body.mine {\n  -webkit-flex-pack: end;\n  -moz-justify-content: flex-end;\n  justify-content: flex-end; }\n/*\n    Align items\n */\n/*\n    Align self\n */\n/*\n    Align content\n */\nform {\n  max-width: 330px;\n  margin: 0 auto; }\n.panel-body.mine {\n  text-align: right; }\n.panel-body .avatar {\n  height: 57px;\n  width: 50px;\n  padding-left: 0;\n  padding-right: 0; }\n.panel-body .msg-text-container {\n  padding-left: 10px;\n  padding-right: 10px; }\n.panel-body .msg-text-container .msg-text {\n    margin-top: 12px; }\n#time {\n  filter: alpha(opacity=0);\n  -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\";\n  -webkit-opacity: 0;\n  -khtml-opacity: 0;\n  -moz-opacity: 0;\n  -ms-opacity: 0;\n  -o-opacity: 0;\n  opacity: 0;\n  transition: opacity .2s ease-in 0s; }\n#time.fade-in {\n    filter: alpha(opacity=100);\n    -ms-filter: \"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)\";\n    -webkit-opacity: 1;\n    -khtml-opacity: 1;\n    -moz-opacity: 1;\n    -ms-opacity: 1;\n    -o-opacity: 1;\n    opacity: 1; }\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/chat.service */ "./src/app/services/chat.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, authService, router, flashMessagesService, chatService) {
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.router = router;
        this.flashMessagesService = flashMessagesService;
        this.chatService = chatService;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.checkLoggedIn();
        this.loginForm = this.formBuilder.group({
            username: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(14)]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4)]]
        });
    };
    LoginComponent.prototype.checkLoggedIn = function () {
        if (this.authService.loggedIn()) {
            this.router.navigate(['chat/chat-room']);
        }
    };
    LoginComponent.prototype.onLoginSubmit = function () {
        var _this = this;
        this.authService.authenticateUser(this.loginForm.value)
            .subscribe(function (data) {
            if (data.success === true) {
                _this.authService.storeUserData(data.token, data.user);
                _this.chatService.connect(data.user.username);
                _this.router.navigate(['/chat/chat-room']);
            }
            else {
                _this.flashMessagesService.show(data.msg, { cssClass: 'alert-danger', timeout: 3000 });
            }
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_3__["FlashMessagesService"],
            _services_chat_service__WEBPACK_IMPORTED_MODULE_5__["ChatService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/message/message.component.html":
/*!************************************************!*\
  !*** ./src/app/message/message.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"panel message\" [ngClass]=\"{'panel-info': message.mine, 'panel-warning': !message.mine}\">\n  <div class=\"panel-body\" [ngClass]=\"{mine: message.mine}\">\n      <div class=\"msg-text-container\">\n        <!-- <h3 class=\"panel-title\"><strong>{{message.from}}</strong></h3> -->\n        <p class=\"msg-text\">{{message.text}}</p>\n        <h3 class=\"panel-title\" id=\"time\" [ngClass]=\"{'fade-in': fadeTime}\"><small> • {{time}}</small></h3>\n      </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/message/message.component.scss":
/*!************************************************!*\
  !*** ./src/app/message/message.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".panel-info {\n  background-color: #5dc3b5;\n  color: white; }\n\n.panel-warning {\n  border: none; }\n"

/***/ }),

/***/ "./src/app/message/message.component.ts":
/*!**********************************************!*\
  !*** ./src/app/message/message.component.ts ***!
  \**********************************************/
/*! exports provided: MessageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageComponent", function() { return MessageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MessageComponent = /** @class */ (function () {
    function MessageComponent() {
    }
    MessageComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.updateFromNow();
            _this.fadeTime = true;
        }, 2000);
        setInterval(function () {
            _this.updateFromNow();
        }, 60000);
    };
    MessageComponent.prototype.updateFromNow = function () {
        this.time = moment__WEBPACK_IMPORTED_MODULE_1__(this.message.created).fromNow();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MessageComponent.prototype, "message", void 0);
    MessageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-message',
            template: __webpack_require__(/*! ./message.component.html */ "./src/app/message/message.component.html"),
            styles: [__webpack_require__(/*! ./message.component.scss */ "./src/app/message/message.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MessageComponent);
    return MessageComponent;
}());



/***/ }),

/***/ "./src/app/navbar/navbar.component.html":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-default navbar-static-top\">\n  <div class=\"container-fluid\">\n    <!-- Brand and toggle get grouped for better mobile display -->\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\n        <span class=\"sr-only\">Toggle navigation</span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n      </button>\n\n      <a routerLink=\"chat/chat-room\" class=\"navbar-brand\" (click)=\"onNavigate()\">\n        <span><img alt=\"Brand\" src=\"assets/img/brand.ico\" width=\"34px\" height=\"34px\"></span>\n        ChatApp\n      </a>\n    </div>\n\n    <!-- Collect the nav links, forms, and other content for toggling -->\n    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n      <ul class=\"nav navbar-nav navbar-right\">\n        <li *ngIf=\"!authService.loggedIn()\" routerLinkActive=\"active\" (click)=\"onNavigate()\"><a routerLink=\"/login\">Login</a></li>\n        <li *ngIf=\"!authService.loggedIn()\" routerLinkActive=\"active\" (click)=\"onNavigate()\"><a routerLink=\"/register\">Register</a></li>\n        <li *ngIf=\"authService.loggedIn()\" routerLinkActive=\"active\" (click)=\"onNavigate()\"><a routerLink=\"/profile\">Profile</a></li>\n        <li *ngIf=\"authService.loggedIn()\"><a href=\"#\" (click)=\"onLogoutClick()\">Logout</a></li>\n      </ul>\n    </div><!-- /.navbar-collapse -->\n  </div><!-- /.container-fluid -->\n</nav>\n"

/***/ }),

/***/ "./src/app/navbar/navbar.component.scss":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* -------------------------------------------------------------\n  Sass CSS3 Mixins! The Cross-Browser CSS3 Sass Library\n  By: Matthieu Aussaguel, http://www.mynameismatthieu.com, @matthieu_tweets\n\n  List of CSS3 Sass Mixins File to be @imported and @included as you need\n\n  The purpose of this library is to facilitate the use of CSS3 on different browsers avoiding HARD TO READ and NEVER\n  ENDING css files\n\n  note: All CSS3 Properties are being supported by Safari 5\n  more info: http://www.findmebyip.com/litmus/#css3-properties\n\n------------------------------------------------------------- */\n/**\n* SassFlexbox\n* Manage Flexbox in Sass easily.\n*\n* @author     Samuel Marchal (zessx)\n* @version    0.1\n*/\n/*\n    Display\n */\n/*\n    Flex direction\n */\n/*\n    Flex wrap\n */\n/*\n    Flex flow\n */\n/*\n    Order\n */\n/*\n    Flex grow\n */\n/*\n    Flex shrink\n */\n/*\n    Flex basis\n */\n/*\n    Flex\n */\n/*\n    Justify content\n */\n/*\n    Align items\n */\n/*\n    Align self\n */\n/*\n    Align content\n */\n.navbar-default {\n  -ms-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.4);\n  -o-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.4);\n  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.4);\n  background-color: whitesmoke;\n  margin-bottom: 0; }\n.navbar-default a.navbar-brand {\n    padding: 13px 15px;\n    color: black; }\n.navbar-default a.navbar-brand span {\n      margin-right: 4px; }\n.navbar-default .navbar-right > li > a {\n    color: black; }\n.navbar-default .navbar-right .active a {\n    background-color: #5dc3b5;\n    color: white; }\n.navbar-default .navbar-left a {\n    background-color: #5dc3b5; }\n"

/***/ }),

/***/ "./src/app/navbar/navbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/chat.service */ "./src/app/services/chat.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(authService, router, chatService, el) {
        this.authService = authService;
        this.router = router;
        this.chatService = chatService;
        this.el = el;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.onLogoutClick = function () {
        this.chatService.disconnect();
        this.authService.logout();
        this.router.navigate(['/login']);
        this.onNavigate();
        return false;
    };
    NavbarComponent.prototype.onNavigate = function () {
        this.collaspseNav();
    };
    NavbarComponent.prototype.collaspseNav = function () {
        var butt = this.el.nativeElement.querySelector('.navbar-toggle');
        var isCollapsed = this.hasClass(butt, 'collapsed');
        if (isCollapsed === false) {
            butt.click();
        }
    };
    NavbarComponent.prototype.hasClass = function (element, cls) {
        return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    };
    NavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.scss */ "./src/app/navbar/navbar.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_chat_service__WEBPACK_IMPORTED_MODULE_3__["ChatService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.component.html":
/*!************************************************!*\
  !*** ./src/app/profile/profile.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"user\" class=\"container\">\n  <h2 class=\"page-header\"><img src=\"assets/img/generic-avatar.png\" alt=\"avatar\" height=\"114px\" width=\"100px\"> {{user.username}}'s profile</h2>\n  <ul class=\"list-group\">\n    <li class=\"list-group-item\">Username: {{user.username}}</li>\n    <li class=\"list-group-item\">Type: awesome</li>\n  </ul>\n\n</div>\n"

/***/ }),

/***/ "./src/app/profile/profile.component.scss":
/*!************************************************!*\
  !*** ./src/app/profile/profile.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(authService) {
        this.authService = authService;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getProfile().subscribe(function (data) {
            _this.user = data.user;
        }, function (err) {
            console.log(err);
            return false;
        });
    };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.scss */ "./src/app/profile/profile.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/services/auth.guard.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/auth.guard.service.ts ***!
  \************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        if (this.authService.loggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular2-jwt */ "./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular2_jwt__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BASE_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].backendUrl;
var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.apiUrl = BASE_URL + "/users";
    }
    AuthService.prototype.registerUser = function (user) {
        var url = this.apiUrl + '/register';
        // prepare the request
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        var reqBody = user;
        // POST
        var observableReq = this.http.post(url, reqBody, options).map(this.extractData);
        return observableReq;
    };
    AuthService.prototype.authenticateUser = function (user) {
        var url = this.apiUrl + '/authenticate';
        // prepare the request
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        var reqBody = user;
        // POST
        var observableReq = this.http.post(url, reqBody, options).map(this.extractData);
        return observableReq;
    };
    AuthService.prototype.getProfile = function () {
        var url = this.apiUrl + '/profile';
        this.loadCredentials();
        // prepare the request
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.authToken
        });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_1__["RequestOptions"]({ headers: headers });
        // POST
        var observableReq = this.http.get(url, options).map(this.extractData);
        return observableReq;
    };
    AuthService.prototype.storeUserData = function (token, user) {
        localStorage.setItem('token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    };
    AuthService.prototype.getUserData = function () {
        this.loadCredentials();
        var jUser = JSON.parse(this.user);
        var jData = { token: this.authToken, user: jUser };
        return jData;
    };
    AuthService.prototype.loadCredentials = function () {
        var token = localStorage.getItem('token');
        var user = localStorage.getItem('user');
        this.authToken = token;
        this.user = user;
    };
    AuthService.prototype.loggedIn = function () {
        return Object(angular2_jwt__WEBPACK_IMPORTED_MODULE_2__["tokenNotExpired"])();
    };
    AuthService.prototype.logout = function () {
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    };
    AuthService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/chat.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/chat.service.ts ***!
  \******************************************/
/*! exports provided: ChatService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function() { return ChatService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! socket.io-client */ "./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var BASE_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].backendUrl;
var ChatService = /** @class */ (function () {
    function ChatService(authService, http) {
        this.authService = authService;
        this.http = http;
        this.chatUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].chatUrl;
        this.apiUrl = BASE_URL + "/messages";
        this.usersUrl = BASE_URL + "/users";
    }
    ChatService.prototype.connect = function (username, callback) {
        var _this = this;
        if (callback === void 0) { callback = function () { }; }
        // initialize the connection
        this.socket = socket_io_client__WEBPACK_IMPORTED_MODULE_4__(this.chatUrl, { path: '/chat' });
        this.socket.on('error', function (error) {
            console.log('====================================');
            console.log(error);
            console.log('====================================');
        });
        this.socket.on('connect', function () {
            _this.sendUser(username);
            console.log('connected to the chat server');
            callback();
        });
    };
    ChatService.prototype.isConnected = function () {
        if (this.socket != null) {
            return true;
        }
        else {
            return false;
        }
    };
    ChatService.prototype.sendUser = function (username) {
        this.socket.emit('username', { username: username });
    };
    ChatService.prototype.disconnect = function () {
        this.socket.disconnect();
    };
    ChatService.prototype.getConversation = function (name1, name2) {
        var url = this.apiUrl;
        if (name2 !== 'chat-room') {
            var route = '/' + name1 + '/' + name2;
            url += route;
        }
        var authToken = this.authService.getUserData().token;
        // prepare the request
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': authToken
        });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        // POST
        var observableReq = this.http.get(url, options)
            .map(this.extractData);
        return observableReq;
    };
    ChatService.prototype.getUserList = function () {
        var url = this.usersUrl;
        var authToken = this.authService.getUserData().token;
        // prepare the request
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': authToken
        });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        // POST
        var observableReq = this.http.get(url, options)
            .map(this.extractData);
        return observableReq;
    };
    ChatService.prototype.receiveMessage = function () {
        var _this = this;
        var observable = new rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"](function (observer) {
            _this.socket.on('message', function (data) {
                observer.next(data);
            });
        });
        return observable;
    };
    ChatService.prototype.receiveActiveList = function () {
        var _this = this;
        var observable = new rxjs_Observable__WEBPACK_IMPORTED_MODULE_1__["Observable"](function (observer) {
            _this.socket.on('active', function (data) {
                observer.next(data);
            });
        });
        return observable;
    };
    ChatService.prototype.sendMessage = function (message, chatWith) {
        this.socket.emit('message', { message: message, to: chatWith });
    };
    ChatService.prototype.getActiveList = function () {
        this.socket.emit('getactive');
    };
    ChatService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    ChatService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], ChatService);
    return ChatService;
}());



/***/ }),

/***/ "./src/app/sign-up/sign-up.component.html":
/*!************************************************!*\
  !*** ./src/app/sign-up/sign-up.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <h2 class=\"page-header\">Register</h2>\n\n  <form [formGroup]=\"signUpForm\"\n    (ngSubmit)=\"onRegisterSubmit()\"\n    >\n\n  <div class=\"form-group\">\n    <label for=\"username\">Username</label>\n\n    <input type=\"text\" name=\"username\" class=\"form-control\"\n      id=\"username\"\n      placeholder=\"Enter your username\"\n      formControlName=\"username\"\n      autofocus=\"true\"\n      >\n  </div>\n\n  <div class=\"form-group\">\n    <label for=\"password\">Password</label>\n\n    <input type=\"password\" name=\"password\" class=\"form-control\"\n      id=\"password\"\n      placeholder=\"Enter your password\"\n      formControlName=\"password\"\n      >\n  </div>\n\n  <div class=\"form-group\">\n    <label for=\"confirmPass\">Confirm password</label>\n\n      <input type=\"password\" name=\"confirmPass\" class=\"form-control\"\n      id=\"confirmPass\"\n      placeholder=\"Confirm password\"\n      formControlName=\"confirmPass\"\n      >\n  </div>\n\n  <div class=\"form-group\">\n    <button [disabled]=\"signUpForm.invalid\"\n      type=\"submit\"\n      class=\"btn btn-primary\"\n      name=\"register\"\n      >Register\n    </button>\n  </div>\n\n  <a [routerLink]=\"['/login']\">Already have an account?</a>\n\n  </form>\n\n</div>\n"

/***/ }),

/***/ "./src/app/sign-up/sign-up.component.scss":
/*!************************************************!*\
  !*** ./src/app/sign-up/sign-up.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/sign-up/sign-up.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sign-up/sign-up.component.ts ***!
  \**********************************************/
/*! exports provided: SignUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpComponent", function() { return SignUpComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular2-flash-messages */ "./node_modules/angular2-flash-messages/index.js");
/* harmony import */ var angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SignUpComponent = /** @class */ (function () {
    function SignUpComponent(formBuilder, flashMessages, router, authService) {
        this.formBuilder = formBuilder;
        this.flashMessages = flashMessages;
        this.router = router;
        this.authService = authService;
    }
    SignUpComponent.prototype.ngOnInit = function () {
        this.checkLoggedIn();
        this.signUpForm = this.formBuilder.group({
            username: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(14)]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4)]],
            confirmPass: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(4)]]
        });
    };
    SignUpComponent.prototype.checkLoggedIn = function () {
        if (this.authService.loggedIn()) {
            this.router.navigate(['/']);
        }
    };
    SignUpComponent.prototype.onRegisterSubmit = function () {
        var _this = this;
        this.authService.registerUser(this.signUpForm.value)
            .subscribe(function (data) {
            if (data.success === true) {
                _this.flashMessages.show(data.msg, { cssClass: 'alert-success', timeout: 3000 });
                _this.router.navigate(['/login']);
            }
            else {
                _this.flashMessages.show(data.msg, { cssClass: 'alert-danger', timeout: 3000 });
            }
        });
    };
    SignUpComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sign-up',
            template: __webpack_require__(/*! ./sign-up.component.html */ "./src/app/sign-up/sign-up.component.html"),
            styles: [__webpack_require__(/*! ./sign-up.component.scss */ "./src/app/sign-up/sign-up.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            angular2_flash_messages__WEBPACK_IMPORTED_MODULE_2__["FlashMessagesService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], SignUpComponent);
    return SignUpComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var BASE_URL = 'http://localhost:3000';
var environment = {
    production: false,
    chatUrl: BASE_URL,
    backendUrl: BASE_URL
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/mo7amd/projects/mean-chat-app-2/client/src/main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** ws (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map