const mongo = require('mongoose');

const MessageSchema = mongo.Schema({
    created: {
        type: String,
        require: true
    },
    from: {
        type: String,
        require: true
    },
    text: {
        type: String,
        require: true
    },
    conversationId: {
        type: String,
        require: true
    },
    inChatRoom: {
        type: Boolean,
        require: false
    }
})


MessageSchema.statics.addMessage = (message, callback) => {
    message.save(callback);
}

MessageSchema.statics.getMessages = (callback) => {
    Message.find({}, callback);
}

MessageSchema.statics.getMessagesByConv = (id, callback) => {
    Message.find({conversationId: id}, callback);
}

const Message = mongo.model('Message', MessageSchema);


module.exports = Message;